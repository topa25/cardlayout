import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
 
public class CLayout {
 
    public static void main(String[] args) {
 
        final JPanel cards; 
        final String FIRST = "FIRST";
        final String NEXT = "NEXT";
        final String PREVIOUS = "PREVIOUS";
        final String LAST = "LAST";
 
        JFrame frame = new JFrame("FARBEN");
 
        //Create the "cards".
        JPanel card1 = new JPanel();
        card1.add(new JLabel("GR�N"));
        card1.setBackground(Color.GREEN);;
 
        JPanel card2 = new JPanel();
        card2.add(new JLabel("BLAU"));
        card2.setBackground(Color.BLUE);
 
        JPanel card3 = new JPanel();
        card3.add(new JLabel("ORANGE"));
        card3.setBackground(Color.ORANGE);
        
        JPanel card4 = new JPanel();
        card4.add(new JLabel("PINK"));
        card4.setBackground(Color.PINK);
        
        JPanel card5 = new JPanel();
        card5.add(new JLabel("ROT"));
        card5.setBackground(Color.RED);
        
        JPanel card6 = new JPanel();
        card6.add(new JLabel("GELB"));
        card6.setBackground(Color.YELLOW);
 
        //Create the panel that contains the "cards".
        cards = new JPanel(new CardLayout());
        cards.add(card1, "card1");
        cards.add(card2, "card2");
        cards.add(card3, "card3");
        cards.add(card4, "card4");
        cards.add(card5, "card5");
        cards.add(card6, "card6");
 
        class ControlActionListenter implements ActionListener {
            public void actionPerformed(ActionEvent e) {
                CardLayout cl = (CardLayout) (cards.getLayout());
                String cmd = e.getActionCommand();
                if (cmd.equals(FIRST)) {
                    cl.first(cards);
                } else if (cmd.equals(NEXT)) {
                    cl.next(cards);
                } else if (cmd.equals(PREVIOUS)) {
                    cl.previous(cards);
                } else if (cmd.equals(LAST)) {
                    cl.last(cards);
                }
            }
        }
        ControlActionListenter cal = new ControlActionListenter();
 
        JButton btn1 = new JButton("First");
        btn1.setActionCommand(FIRST);
        btn1.addActionListener(cal);
 
        JButton btn2 = new JButton("Next");
        btn2.setActionCommand(NEXT);
        btn2.addActionListener(cal);
 
        JButton btn3 = new JButton("Previous");
        btn3.setActionCommand(PREVIOUS);
        btn3.addActionListener(cal);
 
        JButton btn4 = new JButton("Last");
        btn4.setActionCommand(LAST);
        btn4.addActionListener(cal);
 
        JPanel controlButtons = new JPanel();
        controlButtons.add(btn1);
        controlButtons.add(btn2);
        controlButtons.add(btn3);
        controlButtons.add(btn4);
 
        Container pane = frame.getContentPane();
        pane.add(cards, BorderLayout.CENTER);
        pane.add(controlButtons, BorderLayout.PAGE_END);
 
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300,120);
        frame.setVisible(true);
    }
}